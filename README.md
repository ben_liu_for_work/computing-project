# Computing Project: Model Of Misinformation And Retraction On Vaccination Opinion #

## Part1 Individual Model ##
### Dependency ### 
1. Java 1.7 and above
2. jfreechart-1.0.19

### Usage ### 
1. Change the parameters in the file called "ModelConfig.java" as you want
2. Run the model in the file called "IndividualModelRun.java"