package ControlledIndividualModel;

import java.util.ArrayList;
import java.util.Collections;

public class TestSample {
	public static void main(String[] args) {
		ArrayList<Double> list7 = new ArrayList<Double>();
		ArrayList<Double> list8 = new ArrayList<Double>();
		
		double score7 = 0;
		double score8 = 0;
		
		list7.add(-11.66);
		list7.add(-4.332584217317053);
		list7.add(3.0);
		list7.add(3.0);
		list7.add(3.0);
		list7.add(3.0);
		list7.add(3.0);
		
		list8.add(-11.66);
		list8.add(-4.332584217317053);
		list8.add(-1.6098873070450104);
		list8.add(3.0);
		list8.add(3.0);
		list8.add(3.0);
		list8.add(3.0);
		list8.add(3.0);
		
		for(int i=0; i<1000; i++){
			Collections.shuffle(list7);
			for(int j=0; j<4; j++){
				score7 += list7.get(j);
			}
			
			Collections.shuffle(list8);
			for(int j=0; j<4; j++){
				score8 += list8.get(j);
			}
		}
		
		System.out.println("Average score for list 7 is :" + score7/1000);
		System.out.println("Average score for list 8 is :" + score8/1000);
	}
}
