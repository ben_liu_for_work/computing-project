package ControlledIndividualModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Experiments {
	/*
	 * Recover EckerModel
	 */
	public static HashMap<String, Object> recoverEckerModel(int nMI, int nR){
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		
		ArrayList<Token> tokens = new ArrayList<Token>();
		ArrayList<Fact> negFactsRetracted = new ArrayList<Fact>();
		
		Fact posiFact = new Fact(Type.POSITIVE);
		Fact negFact = new Fact(Type.NEGATIVE);
		
		// Add negative tokens
		for (int i=0; i<nMI; i++)
			tokens.add(new Token(negFact, ModelConfig.ALPHA * Math.exp(-i * ModelConfig.LAMDA)));
		
		// Fill it full with positive tokens with weight 0
		while(tokens.size() < ModelConfig.MAXIMUM_NUMBER_OF_TOKENS_IN_HEAD)
			tokens.add(new Token(posiFact, 0));
		
		// Set retractions
		for (int i=0; i<nR; i++)
			negFactsRetracted.add(negFact);
		
		// Set the hashMap
		hashMap.put("tokens", tokens);
		hashMap.put("negFactsRetracted", negFactsRetracted);
		
		return hashMap;
	}
	/*
	 * No-timing experiments
	 */
	public static HashMap<String, Object> IntroduceNonZeroWeightForPosTokens(int nMI, int nR){
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		
		ArrayList<Token> tokens = new ArrayList<Token>();
		ArrayList<Fact> negFactsRetracted = new ArrayList<Fact>();
		
		Fact posiFact = new Fact(Type.POSITIVE);
		Fact negFact = new Fact(Type.NEGATIVE);
		
		// Add negative tokens
		for (int i=0; i<nMI; i++)
			tokens.add(new Token(negFact, ModelConfig.ALPHA * Math.exp(-i * ModelConfig.LAMDA)));
		
		// Fill it full with positive tokens with weight 0
		while(tokens.size() < ModelConfig.MAXIMUM_NUMBER_OF_TOKENS_IN_HEAD)
			tokens.add(new Token(posiFact, 0));
		
		// Set retractions
		for (int i=0; i<nR; i++)
			negFactsRetracted.add(negFact);
		
		// Set the hashMap
		hashMap.put("tokens", tokens);
		hashMap.put("negFactsRetracted", negFactsRetracted);
		
		return hashMap;
	}
	
	// Default weight for predefined negative tokens is 11.66
	public static HashMap<String, Object> introduceNegTokens(int nMI, int nR, int nN){
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		
		ArrayList<Token> tokens = new ArrayList<Token>();
		ArrayList<Fact> negFactsRetracted = new ArrayList<Fact>();
		
		Fact posiFact = new Fact(Type.POSITIVE);
		Fact negFact = new Fact(Type.NEGATIVE);
		
		// Add negative tokens
		for (int i=0; i<nMI; i++)
			tokens.add(new Token(negFact, ModelConfig.ALPHA * Math.exp(-i * ModelConfig.LAMDA)));
		
		// Fill it full with negative tokens with weight 11.66
		while(tokens.size() < ModelConfig.MAXIMUM_NUMBER_OF_TOKENS_IN_HEAD){
			if(nN > 0){
				tokens.add(new Token(new Fact(Type.NEGATIVE), ModelConfig.ALPHA));
				nN--;
			}
			else
				tokens.add(new Token(posiFact, 0));
		}
			
		// Set retractions
		for (int i=0; i<nR; i++)
			negFactsRetracted.add(negFact);
		
		// Set the hashMap
		hashMap.put("tokens", tokens);
		hashMap.put("negFactsRetracted", negFactsRetracted);
		
		return hashMap;
	}
	
	// Introduce positive tokens with weight 0, which means neutral tokens/empty tokens
	public static HashMap<String, Object> introduceDifferentNumOfPositiveTokens(int nMI, int nR, int nP){
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		
		ArrayList<Token> tokens = new ArrayList<Token>();
		ArrayList<Fact> negFactsRetracted = new ArrayList<Fact>();
		
		Fact posiFact = new Fact(Type.POSITIVE);
		Fact negFact = new Fact(Type.NEGATIVE);
		
		// Add negative tokens
		for (int i=0; i<nMI; i++)
			tokens.add(new Token(negFact, ModelConfig.ALPHA * Math.exp(-i * ModelConfig.LAMDA)));
		
		// Fill it with positive tokens with weight 0 according to parameter nP
		while(tokens.size() < ModelConfig.MAXIMUM_NUMBER_OF_TOKENS_IN_HEAD &&
				nP > 0){
			tokens.add(new Token(posiFact, 0));
			nP--;
		}
		
		// Set retractions
		for (int i=0; i<nR; i++)
			negFactsRetracted.add(negFact);
		
		// Set the hashMap
		hashMap.put("tokens", tokens);
		hashMap.put("negFactsRetracted", negFactsRetracted);
		
		return hashMap;
	}
	
	public static HashMap<String, Object> IntroduceDifferentFactTokens(int nMI, int nR){
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		
		ArrayList<Token> tokens = new ArrayList<Token>();
		ArrayList<Fact> negFactsRetracted = new ArrayList<Fact>();
		
		Fact posiFact = new Fact(Type.POSITIVE);
		
		// Create negative facts and corresponding tokens
		ArrayList<Fact> negFacts = new ArrayList<Fact>();
		
		for(int i=0; i<nMI; i++){
			Fact negFact = new Fact(Type.NEGATIVE);
			Token negToken = new Token(negFact, ModelConfig.ALPHA);
			
			negFacts.add(negFact);
			tokens.add(negToken);
		}
		
		
		// Fill it full with positive tokens with weight 0
		while(tokens.size() < ModelConfig.MAXIMUM_NUMBER_OF_TOKENS_IN_HEAD)
			tokens.add(new Token(posiFact, 0));
		
		// Set retractions
		if (nMI == 1 && nR == 3){
			for (int i=0; i<nR; i++)
				negFactsRetracted.add(negFacts.get(0));
		}else{
			for (int i=0; i<nR; i++)
				negFactsRetracted.add(negFacts.get(i));
		}
		
		// Set the hashMap
		hashMap.put("tokens", tokens);
		hashMap.put("negFactsRetracted", negFactsRetracted);
		
		return hashMap;
	}
	
	/*
	 * No-timing experiments ---- consider the "Replacement" concept
	 */
	public static HashMap<String, Object> replacePreDefinedTokensAccordingToWeight(int nMI, int nR, ArrayList<Token> preDefinedTokens){
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		
		ArrayList<Token> tokens = new ArrayList<Token>();
		ArrayList<Fact> negFactsRetracted = new ArrayList<Fact>();
		
		Fact posiFact = new Fact(Type.POSITIVE);
		Fact negFact = new Fact(Type.NEGATIVE);
		
		int availableEmptySlots = ModelConfig.MAXIMUM_NUMBER_OF_TOKENS_IN_HEAD - preDefinedTokens.size();
		
		// Add negative tokens
		for (int i=0; i<nMI; i++){
			if (availableEmptySlots > 0){
				tokens.add(new Token(negFact, ModelConfig.ALPHA * Math.exp(-i * ModelConfig.LAMDA)));
				availableEmptySlots--;
			}else{
				Token preDefinedTokenReplaced = Individual.selectOneFactRandomlyReversely(preDefinedTokens);
				
				if (preDefinedTokenReplaced.getWeight() < ModelConfig.ALPHA * Math.exp(-i * ModelConfig.LAMDA)){
					// Find the preDefinedTokenReplaced and then replace it with a negative token
					for (int j=0; j<preDefinedTokens.size(); j++){
						if (preDefinedTokens.get(j) == preDefinedTokenReplaced){
							preDefinedTokens.set(j, new Token(negFact, ModelConfig.ALPHA * Math.exp(-i * ModelConfig.LAMDA)));
							break;
						}			
					}
				}
			}
		}
		
		// Combine the tokens
		tokens.addAll(preDefinedTokens);
		
		// Fill it full with positive tokens with weight 0
		while(tokens.size() < ModelConfig.MAXIMUM_NUMBER_OF_TOKENS_IN_HEAD)
			tokens.add(new Token(posiFact, 0));
		
		// Set retractions
		for (int i=0; i<nR; i++)
			negFactsRetracted.add(negFact);
		
		// Set the hashMap
		hashMap.put("tokens", tokens);
		hashMap.put("negFactsRetracted", negFactsRetracted);
		
		return hashMap;
	}
	
	public static HashMap<String, Object> replacePreDefinedTokensRandomly(int nMI, int nR, ArrayList<Token> preDefinedTokens){
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		
		ArrayList<Token> tokens = new ArrayList<Token>();
		ArrayList<Fact> negFactsRetracted = new ArrayList<Fact>();
		
		Fact posiFact = new Fact(Type.POSITIVE);
		Fact negFact = new Fact(Type.NEGATIVE);
		
		int availableEmptySlots = ModelConfig.MAXIMUM_NUMBER_OF_TOKENS_IN_HEAD - preDefinedTokens.size();
		
		// Add negative tokens
		for (int i=0; i<nMI; i++){
			if (availableEmptySlots > 0){
				tokens.add(new Token(negFact, ModelConfig.ALPHA * Math.exp(-i * ModelConfig.LAMDA)));
				availableEmptySlots--;
			}else{
				Collections.shuffle(preDefinedTokens);
				
				preDefinedTokens.set(0, new Token(negFact, ModelConfig.ALPHA * Math.exp(-i * ModelConfig.LAMDA)));
			}
		}
		
		// Combine the tokens
		tokens.addAll(preDefinedTokens);
		
		// Fill it full with positive tokens with weight 0
		while(tokens.size() < ModelConfig.MAXIMUM_NUMBER_OF_TOKENS_IN_HEAD)
			tokens.add(new Token(posiFact, 0));
		
		// Set retractions
		for (int i=0; i<nR; i++)
			negFactsRetracted.add(negFact);
		
		// Set the hashMap
		hashMap.put("tokens", tokens);
		hashMap.put("negFactsRetracted", negFactsRetracted);
		
		return hashMap;
	}
	
	/*
	 * Timing experiments
	 */	
	public static HashMap<String, Object> differentRetractionTimeIntroduced(int retractionTime, int endTicks){
		Fact negFact = new Fact(Type.NEGATIVE);
		Token negToken = new Token(negFact, ModelConfig.ALPHA, 1);
		
		Fact retractedFact = new Fact(Type.RETRACTION, negFact);
		Token retractedToken = new Token(retractedFact, ModelConfig.ALPHA, retractionTime);
		
		ArrayList<Token> tokens = new ArrayList<Token>();
		tokens.add(negToken);
		tokens.add(retractedToken);
		
		HashMap<String, Object> hashmap = new HashMap<String, Object>();
		hashmap.put("tokens", tokens);
		hashmap.put("endTicks", endTicks);
		
		return hashmap;
	}
	
	// Retraction immediately after misinformation or Retraction after all MI presented
	// (default interval == 3)
	public static HashMap<String, Object> differentRetractionPattern(int endTicks, String pattern){
		ArrayList<Token> tokens = new ArrayList<Token>();
		
		Fact negFact = new Fact(Type.NEGATIVE);
		
		Fact retractedFact = new Fact(Type.RETRACTION, negFact);
		
		if (pattern.equals("Immediate")){
			tokens.add(new Token(negFact, ModelConfig.ALPHA, 1));
			tokens.add(new Token(retractedFact, ModelConfig.ALPHA, 1));
			
			tokens.add(new Token(negFact, ModelConfig.ALPHA * Math.exp(-1 * ModelConfig.LAMDA), 4));
			tokens.add(new Token(retractedFact, ModelConfig.ALPHA, 4));
			
			tokens.add(new Token(negFact, ModelConfig.ALPHA * Math.exp(-2 * ModelConfig.LAMDA), 7));
			tokens.add(new Token(retractedFact, ModelConfig.ALPHA, 7));
		}else{
			tokens.add(new Token(negFact, ModelConfig.ALPHA, 1));
			tokens.add(new Token(negFact, ModelConfig.ALPHA * Math.exp(-1 * ModelConfig.LAMDA), 4));
			tokens.add(new Token(negFact, ModelConfig.ALPHA * Math.exp(-2 * ModelConfig.LAMDA), 7));
			
			tokens.add(new Token(retractedFact, ModelConfig.ALPHA, 10));
			tokens.add(new Token(retractedFact, ModelConfig.ALPHA, 10));
			tokens.add(new Token(retractedFact, ModelConfig.ALPHA, 10));
		}
		
		
		HashMap<String, Object> hashmap = new HashMap<String, Object>();
		hashmap.put("tokens", tokens);
		hashmap.put("endTicks", endTicks);
		
		return hashmap;
	}
	
	// Retraction order(default interval == 3)
	public static int[][] orders = {{0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 0, 1}, {2, 1, 0}};
	
	public static HashMap<String, Object> differentRetractionOrder(int endTicks, int[] order){
		ArrayList<Token> tokens = new ArrayList<Token>();
		
		// Create negative tokens
		Fact negFact1 = new Fact(Type.NEGATIVE);
		Fact negFact2 = new Fact(Type.NEGATIVE);
		Fact negFact3 = new Fact(Type.NEGATIVE);
		
		tokens.add(new Token(negFact1, ModelConfig.ALPHA * 1, 1));
		tokens.add(new Token(negFact2, ModelConfig.ALPHA * 0.5, 4));
		tokens.add(new Token(negFact3, ModelConfig.ALPHA * 0.1, 7));
		
		// Create retraction tokens
		for(int i=0; i< order.length; i++){
			Fact retractionFact = new Fact(Type.RETRACTION, tokens.get(order[i]).getBelongTo());
			
			tokens.add(new Token(retractionFact, ModelConfig.ALPHA, 10 + 3 * i));
		}
		
		HashMap<String, Object> hashmap = new HashMap<String, Object>();
		hashmap.put("tokens", tokens);
		hashmap.put("endTicks", endTicks);
		
		return hashmap;
	}
	
	// Retraction with no target
	public static HashMap<String, Object> differentOrderOfNoTargetRetraction(int endTicks, int[] order){
		ArrayList<Token> tokens = new ArrayList<Token>();
		
		// Create negative tokens
		Fact negFact1 = new Fact(Type.NEGATIVE);
		Fact negFact2 = new Fact(Type.NEGATIVE);
		Fact negFact3 = new Fact(Type.NEGATIVE);
		
		tokens.add(new Token(negFact1, ModelConfig.ALPHA, 1));
		tokens.add(new Token(negFact2, ModelConfig.ALPHA, 4));
		tokens.add(new Token(negFact3, ModelConfig.ALPHA, 7));
		
		// Create retraction tokens
		for(int i=0; i< order.length; i++){
			Fact retractionFact = new Fact(Type.RETRACTION, tokens.get(order[i]).getBelongTo());
			
			tokens.add(new Token(retractionFact, ModelConfig.ALPHA*0.5, 2 + 3 * i));
		}
		
		HashMap<String, Object> hashmap = new HashMap<String, Object>();
		hashmap.put("tokens", tokens);
		hashmap.put("endTicks", endTicks);
		
		return hashmap;
	}
}
