package ControlledIndividualModel;

import java.util.UUID;

enum Type {
	POSITIVE, NEGATIVE, RETRACTION
}

public class Fact {
	private String id;
	private Type type;
	
	private Fact retractOn;
	
	public Fact(Type type){
		this.id = UUID.randomUUID().toString();
		this.type = type;
		
		this.retractOn = null;
	}
	
	public Fact(Type type, Fact retractOn){
		this.id = UUID.randomUUID().toString();
		this.type = type;
		
		this.retractOn = retractOn;
	}

	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}
	
	public Fact getRetractOn() {
		return retractOn;
	}

	@Override
	public String toString() {
		return "Fact [id=" + id + ", type=" + type + "]";
	}
}
