package ControlledIndividualModel;

import java.util.Random;

public class ModelConfig {
	// System-related constant
	public static final Random random = new Random();
		
	// Model-related constant
	public static final int MAXIMUM_NUMBER_OF_TOKENS_IN_HEAD = 12;
	public static final int NUMBER_OF_SAMPLE = 4;
	
	public static final double RETRACTION_REDUCTION_FACTOR = 0.39;
	
	public static final int MAXIMUM_FACT_WEIGHT = 12;
	public static final int MAXIMUM_FACT_LIFT_TICKS = 100;
	
	public static final boolean IS_WEIGHT_REDUCED_WITH_REDUCTION_OF_LIFE = true;
	
	// Parameters in the reduction-across-repetitions equation
	public static final double LAMDA = 0.99;
	public static final double ALPHA = 11.66;
	
	// Repetition of the Model
	public static final int NUMBER_OF_REPETITON = 1000;
	
	// The maximum period of time for an individual to remember a token(Days)
	public static final double MAXIMUM_MEMORY_PERIOD = 8;
	
}
