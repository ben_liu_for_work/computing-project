package ControlledIndividualModel;

import java.util.UUID;

public class Token implements Comparable<Token> {
	private String id;
	private Fact belongTo;
	private Type tokenType;
	
	private double weight;
	
	private boolean isRetracted;
	private int retractionTagAge;
	
	private int createdTime;
	private double age;
	
	public Token(Fact belongTo, double weight) {
		this.id = UUID.randomUUID().toString();
		this.belongTo = belongTo;
		this.tokenType = belongTo.getType();
		
		this.weight = weight;
		this.isRetracted = false;
		
		this.createdTime = -1;
		this.age = -1;
		this.retractionTagAge = -1;
		
	}
	
	public Token(Fact belongTo, double weight, int createdTime) {
		this.id = UUID.randomUUID().toString();
		this.belongTo = belongTo;
		this.tokenType = belongTo.getType();
		
		this.weight = weight;
		
		this.isRetracted = false;
		this.retractionTagAge = -1;
		
		this.createdTime = createdTime;
		this.age = 0;
	}
	
	public void increaseAgeByOne() {
		this.age++;
	}
	
	/*
	 * Getter And Setter
	 */
	public String getId() {
		return id;
	}
	
	public boolean isRetracted() {
		return isRetracted;
	}
	
	public double getAge() {
		return age;
	}

	public void setAge(double age) {
		this.age = age;
	}

	public void setRetracted(boolean isRetracted) {
		this.isRetracted = isRetracted;
	}
	
	public double getWeight() {
		return weight;
	}
	
	public void setWeight(double weight) {
		this.weight = weight;
	}

	public Type getCorresFactType(){
		return this.belongTo.getType();
	}
	
	public String getCorresFactId(){
		return this.belongTo.getId();
	}

	public int getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(int createdTime) {
		this.createdTime = createdTime;
	}

	public Fact getBelongTo() {
		return belongTo;
	}

	public void setBelongTo(Fact belongTo) {
		this.belongTo = belongTo;
	}

	public Type getTokenType() {
		return tokenType;
	}

	public void setTokenType(Type tokenType) {
		this.tokenType = tokenType;
	}
	
	public int getRetractionTagAge() {
		return retractionTagAge;
	}

	public void setRetractionTagAge(int retractionTagAge) {
		this.retractionTagAge = retractionTagAge;
	}

	@Override
	public int compareTo(Token token) {
		return Double.compare(this.weight, token.weight);
	}

	@Override
	public String toString() {
		return "Token [id=" + id + ", belongTo=" + belongTo + ", tokenType=" + tokenType + ", weight=" + weight
				+ ", isRetracted=" + isRetracted + ", retractionTagAge=" + retractionTagAge + ", createdTime="
				+ createdTime + ", age=" + age + "]";
	}
}
