package ControlledIndividualModel;

import java.util.ArrayList;
import java.util.HashMap;

public class TestRandomSelection {

	public static void main(String[] args) {
		ArrayList<Token> tokens = new ArrayList<Token>();

		Fact posiFact1 = new Fact(Type.POSITIVE);
		Token t1 = new Token(posiFact1, 1);
		Token t2 = new Token(posiFact1, 2);
		Token t3 = new Token(posiFact1, 4);
		
		t1.setAge(1);
		t2.setAge(2);
		t3.setAge(4);
		
		tokens.add(t1);
		tokens.add(t2);
		tokens.add(t3);
		
		// print the tokens
		for(int i=0; i<tokens.size(); i++){
			System.out.println(tokens.get(i));
		}
		
		// create counter
		HashMap<String, Integer> counter = new HashMap<String, Integer>();
		
		// Reverse the weight
		for(int i=0; i<tokens.size(); i++){
			counter.put(tokens.get(i).getId(), 0);
		}
		
		for(int i=0; i<2000; i++){
			Token token = Individual.selectOneFactRandomlyReversely_Age(tokens);
			counter.put(token.getId(), counter.get(token.getId()) + 1);
		}
		
		// print the counter
		System.out.println(counter);
		
	}

}
