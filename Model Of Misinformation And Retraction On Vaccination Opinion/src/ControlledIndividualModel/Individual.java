package ControlledIndividualModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

public class Individual {
	private String id;
	private ArrayList<Token> tokens;
	
	// An array to contain all the negative fact retracted(allow duplication)
	private ArrayList<Fact> negFactRetracted;
	
	private int endTicks;
	
	public Individual( ArrayList<Token> tokens) {
		this.id = UUID.randomUUID().toString();
		this.tokens = tokens;
		
		this.negFactRetracted = new ArrayList<Fact>();
	}
	
	public Individual( ArrayList<Token> tokens, int endTicks) {
		this.id = UUID.randomUUID().toString();
		this.tokens = tokens;
		
		this.negFactRetracted = new ArrayList<Fact>();
		this.endTicks = endTicks;
	}
	
	public Individual( ArrayList<Token> tokens, ArrayList<Fact> negFactRetracted) {
		this.id = UUID.randomUUID().toString();
		this.tokens = tokens;
		
		this.negFactRetracted = negFactRetracted;
	}

	// Decide the attitude towards vaccination -- No Timing
	public double decideAttitude(){
		double score = 0;
		ArrayList<Token> tokensInHead = new ArrayList<Token> (tokens);
		ArrayList<Fact> copiedNegFactRetracted = new ArrayList<Fact>(this.negFactRetracted);
		/*
		 *  not-weighted Sample from the tokens
		 */
		// Shuffle
		Collections.shuffle(tokensInHead);
		ArrayList<Token> shuffledTokens = tokensInHead;
		
		// Sample
		for(int i=0; i<ModelConfig.NUMBER_OF_SAMPLE && i<shuffledTokens.size(); i++){
			Token sampledToken = shuffledTokens.get(i);
			Fact corresFact = sampledToken.getBelongTo();
			
			/*System.out.println();
			System.out.println(sampledToken);*/
			
			// If this sampled token is retracted
			if (copiedNegFactRetracted.contains(corresFact)){
				score += -sampledToken.getWeight() * ModelConfig.RETRACTION_REDUCTION_FACTOR;
				
				sampledToken.setRetracted(true);
				copiedNegFactRetracted.remove(corresFact);
				
				//System.out.println(negFactRetracted);
			}else{
				if (sampledToken.getTokenType() == Type.POSITIVE)
					score += sampledToken.getWeight();
				else
					score += -sampledToken.getWeight();
			}
			
			//System.out.println(score);
		}
		
		return score;
	}
	
	// Decide the attitude towards vaccination -- Timing
	public double decideAttitude_Timing(){
		double score = 0;
		ArrayList<Token> copiedTokens = new ArrayList<Token>(tokens);
		ArrayList<Token> sampledTokens = new ArrayList<Token>();
		
		// Run the model first
		ArrayList<Token> finalTokens = runTimingModel(copiedTokens, this.endTicks);
		
		// Add neutral tokens to fill the memory
		while(finalTokens.size() < ModelConfig.MAXIMUM_NUMBER_OF_TOKENS_IN_HEAD){
			Token neutralToken = new Token(new Fact(Type.POSITIVE), 0);
			
			// All neutral tokens starts counting at tick 1.
			// Then all have age endTick - 1
			neutralToken.setAge(this.endTicks - 1);
			
			finalTokens.add(neutralToken);
		}
		
		/*System.out.println("final tokens: ");
		for (int j=0; j<finalTokens.size(); j++){
			System.out.println("Token " + (j+1) + " is: " + finalTokens.get(j));
		}*/
		
		// Sample according to age of each token
		for(int i=0; i<ModelConfig.NUMBER_OF_SAMPLE && i<finalTokens.size(); i++){
			Token sampledToken = selectOneFactRandomlyReversely_Age(finalTokens);
			sampledTokens.add(sampledToken);
			
			finalTokens.remove(sampledToken);
			
		}
		
		/*System.out.println("Sampled Tokens: ");
		for (int j=0; j<sampledTokens.size(); j++){
			System.out.println("Token " + (j+1) + " is: " + sampledTokens.get(j));
		}*/
		
		/*
		 *  Compute score
		 */
		// Iterate each token in the sample.(i.e. check whether is retracted, if so, then make it effective according to retraction age)
		for(int i=0; i<sampledTokens.size(); i++){
			Token token = sampledTokens.get(i);
			if (token.isRetracted()){
				double sampled_prob = computeProbByAge(token.getRetractionTagAge());
				if (Math.random() < sampled_prob)
					score -= token.getWeight() * ModelConfig.RETRACTION_REDUCTION_FACTOR;
				else{
					score -= token.getWeight();
				}
			}else{
				if(token.getTokenType() == Type.POSITIVE)
					score += token.getWeight();
				else
					score -= token.getWeight();
			}
		}
		
		return score;
	}
	
	/*
	 * Helper Functions
	 */
	
	// Probability function based on the sigmoid function
	// to decide the probability of making retraction tag effective.
	public static double computeProbByAge(double x) {
		//System.out.println(x);
		//System.out.println((1/( 1 + Math.pow(Math.E,(x - MAXIMUM_MEMORY_PERIOD/2)))));
	    return (1/( 1 + Math.pow(Math.E,(x - ModelConfig.MAXIMUM_MEMORY_PERIOD/2))));
	}
	
	// Update helper functions for Timing models
	public static ArrayList<Token> runTimingModel(ArrayList<Token> tokens, int endTick){
		ArrayList<Token> tokensInHead = new ArrayList<Token>();
		for (int i=1; i<=endTick; i++){
			updateTokensInHead(tokensInHead);
			
			// If the createTime is equal to the current time, then put
			// it into the head
			ArrayList<Token> tokensConsidered = new ArrayList<Token>() ;
			for (Token token: tokens){
				if (token.getCreatedTime() == i){
					if (token.getCorresFactType() == Type.RETRACTION ){
						Token retractedToken = getRetractedToken(token, tokensInHead);
						if (retractedToken != null){
							// If have target
							retractedToken.setRetracted(true);
							retractedToken.setRetractionTagAge(0);
						}else{
							// If no target
							Token newToken = new Token (token.getBelongTo().getRetractOn(), 
														ModelConfig.MAXIMUM_FACT_WEIGHT - token.getWeight(), i);
							newToken.setRetracted(true);
							newToken.setRetractionTagAge(0);
							
							tokensInHead.add(token);
						}
					}else{
						tokensInHead.add(token);
					}
					
					tokensConsidered.add(token);
				}
			}
			
			// Remove the token already considered
			if (tokensConsidered.size() != 0)
				for(Token consideredToken: tokensConsidered)
					tokens.remove(consideredToken);
		}
		
		return tokensInHead;
	}
	
	public static Token getRetractedToken(Token retractionToken, ArrayList<Token> tokensInHead){
		Fact retractedFact = retractionToken.getBelongTo().getRetractOn();
		for (int i=tokensInHead.size()-1; i>=0;i--){
			Token token = tokensInHead.get(i);
			if (token.getBelongTo() == retractedFact && !token.isRetracted())
				return token;
		}
		return null;
	}
	
	public static void updateTokensInHead(ArrayList<Token> tokensInHead){
		for (int i=0; i<tokensInHead.size(); i++){
			Token token = tokensInHead.get(i);
			
			// Increase the age of the token
			token.increaseAgeByOne();
			
			// Increase the age of the retraction tag if have one
			if (token.isRetracted())
				token.setRetractionTagAge(token.getRetractionTagAge()+1);
		}
	}
	
	// Select one fact from a fact list randomly according to the weight
	public static Token selectOneFactRandomly(ArrayList<Token> list){
		// Compute the total weight of all items together
		double totalWeight = 0.0;
		for (Token i : list)
		{
		    totalWeight += i.getWeight();
		}
		
		// Now choose a random item
		int randomIndex = -1;
		double random = Math.random() * totalWeight;
		for (int i = 0; i < list.size(); i++)
		{
		    random -= list.get(i).getWeight();
		    
		    if (random <= 0.0)
		    {
		        randomIndex = i;
		        break;
		    }
		}
		
		if (randomIndex == -1)
			return null;
		else 
			return list.get(randomIndex);
	}
	
	// According to the weight
	public static Token selectOneFactRandomlyReversely(ArrayList<Token> list){
		// Reverse the weight
		for(int i=0; i<list.size(); i++){
			list.get(i).setWeight(1.0/list.get(i).getWeight());
		}
		
		// Get the token selected randomly and reversely
		Token sampledToken = selectOneFactRandomly(list);
		
		// Reset the weight
		for(int i=0; i<list.size(); i++){
			list.get(i).setWeight(1.0/list.get(i).getWeight());
		}
		
		return sampledToken;
	}
	
	// According to the Age
	public static Token selectOneFactRandomlyReversely_Age(ArrayList<Token> list){
		Token sampledToken = null;
		// Reverse the Age
		for(int i=0; i<list.size(); i++){
			list.get(i).setAge(1.0/list.get(i).getAge());
		}
		
		// Get the token selected randomly and reversely
		double totalWeight = 0.0;
		for (Token i : list)
		{
		    totalWeight += i.getAge();
		}
		
		// Now choose a random item
		int randomIndex = -1;
		double random = Math.random() * totalWeight;
		for (int i = 0; i < list.size(); i++)
		{
		    random -= list.get(i).getAge();
		    
		    if (random <= 0.0)
		    {
		        randomIndex = i;
		        break;
		    }
		}
		
		if (randomIndex == -1)
			sampledToken = null;
		else 
			sampledToken = list.get(randomIndex);
		
		// Reset the Age
		for(int i=0; i<list.size(); i++){
			list.get(i).setAge(1.0/list.get(i).getAge());
		}
		
		return sampledToken;
	}

	public ArrayList<Token> getTokens() {
		return tokens;
	}
}
