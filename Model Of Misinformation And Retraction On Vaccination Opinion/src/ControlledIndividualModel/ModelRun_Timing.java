package ControlledIndividualModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import PaperModel.EckerModel;

public class ModelRun_Timing {
	public static void main(String[] args) {
		double score = 0.0;
		for (int k=0; k<ModelConfig.NUMBER_OF_REPETITON; k++){
					
			HashMap<String, Object> hashMap = Experiments.differentRetractionTimeIntroduced(3, 20);
			
			// Put the tokens into one individual's head
			Individual individual = new Individual((ArrayList<Token>)(hashMap.get("tokens")), 
					(int)hashMap.get("endTicks"));
			/*
			System.out.println();
			System.out.println("Token into the head is: ");
			
			for (int i=0; i<individual.getTokens().size(); i++){
				System.out.println("Token " + (i+1) + " is: " + individual.getTokens().get(i));
			}*/
			
			// Total Score
			score += individual.decideAttitude_Timing();;
		}
		System.out.println("The average score through " + 
				ModelConfig.NUMBER_OF_REPETITON + " Repetitions is : " + 
				score/ModelConfig.NUMBER_OF_REPETITON);
	}
}
