package ControlledIndividualModel;

import java.util.ArrayList;
import java.util.HashMap;

import PaperModel.EckerModel;

public class ModelRun {
	
	private static int positiveTimes = 0;
	private static int negativeTimes = 0;
	
	public static void main(String[] args) {
		
		for (int k=0; k<EckerModel.FULLGRID.length; k++){
			int nMI = EckerModel.FULLGRID[k][0];
			int nR =  EckerModel.FULLGRID[k][1];
			
			// Construct the pre-defined tokens before experiment
			ArrayList<Token> preDefinedTokens = new ArrayList<Token>();
			Fact posiFact = new Fact(Type.POSITIVE);
			
			preDefinedTokens.add(new Token(posiFact, 1));
			preDefinedTokens.add(new Token(posiFact, 1));
			preDefinedTokens.add(new Token(posiFact, 1));
			preDefinedTokens.add(new Token(posiFact, 1));
			preDefinedTokens.add(new Token(posiFact, 1));
					
			HashMap<String, Object> hashMap = Experiments.introduceNegTokens(nMI, nR, 8);
			
			// Put the tokens into one individual's head
			Individual individual = new Individual((ArrayList<Token>)hashMap.get("tokens")
					, (ArrayList<Fact>)hashMap.get("negFactsRetracted"));
			
			/*System.out.println();
			System.out.println("Token into the head is: ");
			
			for (int i=0; i<individual.getTokens().size(); i++){
				System.out.println("Token " + (i+1) + " is: " + individual.getTokens().get(i));
			}*/
			
			//System.out.println((ArrayList<Fact>)hashMap.get("negFactsRetracted"));
			
			// Total Score
			double score = 0.0;
			
			// Run the model several times
			for(int i=0; i<ModelConfig.NUMBER_OF_REPETITON; i++){
				double newScore = individual.decideAttitude();
				
				score += newScore;
			}
			
			System.out.print(score/ModelConfig.NUMBER_OF_REPETITON + ", ");
		
		}
		
	}
	
	// Predefined tokens sequence
	private static void initializeTokens(ArrayList<Token> tokens) {
		// Create multiple facts
		Fact posiFact1 = new Fact(Type.POSITIVE);
		
		Fact negFact1 = new Fact(Type.NEGATIVE);
		Fact negFact2 = new Fact(Type.NEGATIVE);
		
		Fact retractedFactOn1 = new Fact(Type.RETRACTION, negFact1);
		Fact retractedFactOn2 = new Fact(Type.RETRACTION, negFact2);
		
		/*// Add multiple tokens in order without timing
		tokens.add(new Token(posiFact1, 8));
		tokens.add(new Token(retractedFactOn2, 4));
		tokens.add(new Token(negFact1, 4));
		tokens.add(new Token(posiFact1, 1));
		tokens.add(new Token(negFact2, 6));
		tokens.add(new Token(retractedFactOn2,4));
		tokens.add(new Token(negFact2, 3));*/
		
		/*// Add multiple tokens in order with timing
		tokens.add(new Token(posiFact1, 5, 1));
		tokens.add(new Token(posiFact1, 6, 1));
		tokens.add(new Token(retractedFactOn2, 4, 2));
		tokens.add(new Token(posiFact1, 3, 3));
		tokens.add(new Token(negFact2, 6, 5));
		tokens.add(new Token(negFact2, 8, 20));*/
		
	}
	
	// Function used to read token sequence from a .csv file
	// Not used yet
	private static ArrayList<Token> readFromFile() {
		// TODO Auto-generated method stub
		return null;
	}
}
