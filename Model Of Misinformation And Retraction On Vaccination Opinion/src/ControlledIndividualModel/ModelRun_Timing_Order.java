package ControlledIndividualModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class ModelRun_Timing_Order {
	public static void main(String[] args) {
		// Initialize the score
		double score = 0;

		for(int j=0; j<Experiments.orders.length; j++){
			int[] order = Experiments.orders[j];
			score = 0;
			
			// Do decision 1000 times
			for(int i=0; i<ModelConfig.NUMBER_OF_REPETITON; i++){
				// Get tokens after running a certain number of ticks from the experiments
				HashMap<String, Object> hashmap = Experiments.differentOrderOfNoTargetRetraction(10, order);
				
				//System.out.println(hashmap);
				
				// Put the tokens into the Individual's head
				Individual individual = new Individual((ArrayList<Token>)(hashmap.get("tokens")), (int)hashmap.get("endTicks"));
				
				/*for (int k=0; k<individual.getTokens().size(); k++){
					System.out.println("Token " + (k+1) + " is: " + individual.getTokens().get(k));
				}*/
				
				score += individual.decideAttitude_Timing();
			}
			
			System.out.println("The order is " + Arrays.toString(order));
			
			// Print the average
			System.out.println("The average score through " + ModelConfig.NUMBER_OF_REPETITON + " Repetitions is : " + score/ModelConfig.NUMBER_OF_REPETITON);
		}
		
		/*// Print the average
		System.out.print("The average score through " + ModelConfig.NUMBER_OF_REPETITON + " Repetitions is : " + score/ModelConfig.NUMBER_OF_REPETITON);*/
	}
}
