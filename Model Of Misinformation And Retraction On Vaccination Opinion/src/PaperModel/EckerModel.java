package PaperModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import ControlledIndividualModel.Token;

public class EckerModel {
	// [[nMI, nR], .....]
	// nMI: number of misinfo tokens at input
	// nR: number of retractions
	public static final int[][] FULLGRID = {{0, 0},	 {1, 0}, {1, 1}, {1, 3}, {3, 0}, {3, 1}, {3, 3}};
	
	public static final int NSAMPLES = 4;
	public static final int NTOKENS = 12;
	public static final int NREP = 1000;
	
	// proportion of MI reduction
	public static final double PSAMPLENEGTAG = 0.61;
	
	// parameters in the reduction-across-repetitions equation
	public static final double ALPHA = 11.66;
	public static final double LAMDA = 0.99;
	
	public static final ArrayList<Double> EXPERIMENT_DATA = new ArrayList<Double>
		(Arrays.asList(0.39, 4.48, 1.83, 1.87, 5.67, 3.22, 1.70));
		
	
	public static void main(String[] args) {
		//��An array of strength for 3-times repetition of token 
		ArrayList<Double> magnitudeMI = new ArrayList<Double>();
		ArrayList<Double> misinfo = new ArrayList<Double>(Collections.nCopies(FULLGRID.length, 0.0));
		
		// Initialize the magnitudeMI
		for(int i=0; i<3; i++){
			double value = ALPHA * Math.exp(-i * LAMDA);
			magnitudeMI.add(value);
		}
		
		for(int rep=0; rep<NREP; rep++ ){
			for(int i=0; i<FULLGRID.length;i++){
				/*
				 *  each condition in each replication happens here
				 */
				ArrayList<Double> tokens= new ArrayList<Double>(Collections.nCopies(NTOKENS, 0.0));
				
				for(int j=0; j<FULLGRID[i][0]; j++)
					tokens.set(j, magnitudeMI.get(j));
				
				/*
				 *  Sample
				 */
				int sampledRetracts = FULLGRID[i][1];
				
				Collections.shuffle(tokens);
				ArrayList<Double> shuffledTokens = tokens;
				
				ArrayList<Double> sample = new ArrayList<Double>();
				
				for(int k=0; k<NSAMPLES; k++){
					double sampledToken = shuffledTokens.get(k);
					sample.add(sampledToken);
					
					if(sampledToken > 0){ // it is a misinfo
						if (sampledRetracts > 0){
							sample.add(-PSAMPLENEGTAG * sampledToken);
							sampledRetracts--;
						}
					} 	
				}
				
				/*
				 * Add all values of misinfos for this condition
				 */
				// Get sum
				double sum = 0;
				for(int g=0; g<sample.size();g++){
					sum += sample.get(g);
				}
				
				// Set value
				misinfo.set(i, misinfo.get(i) + sum);
			}
		}
		
		// Get the average for each condition
		for(int i=0; i<misinfo.size(); i++)
			misinfo.set(i, misinfo.get(i)/NREP);
		
		// Output 
		System.out.println("Model Prediction: ");
		System.out.println(misinfo);
		System.out.println();
		
		System.out.println("Experiment Result: ");
		System.out.print(EXPERIMENT_DATA);
		
	}

}
