package RandomIndividualModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.UUID;

public class Individual {
	private String id;
	private ArrayList<Fact> facts;
	private boolean isPositive;

	public Individual(){
		this.id = UUID.randomUUID().toString();
		this.facts = Fact.createFacts(ModelConfig.MAXIMUM_NUMBER_OF_FACTS_IN_HEAD);
		
		this.isPositive = ModelConfig.random.nextBoolean();
	}
	
	// Decide the attitude towards vaccination
	public Boolean decideAttitude(){
		// The total weights for positive and negative facts, respectively
		double positiveFactsWeight = 0;
		double negativeFactsWeight = 0;
		
		ArrayList<Fact> sampledPositiveFacts = new ArrayList<Fact>();
		ArrayList<Fact> sampledNegativeFacts = new ArrayList<Fact>();
		
		// Create a new copy of facts
		ArrayList<Fact> copiedFacts = new ArrayList<Fact> (this.facts);
		
		// Sample certain number of facts
		for(int i=0; i < ModelConfig.NUMBER_OF_SAMPLE; i++){
			Fact sampledFact = selectOneFactRandomly(copiedFacts);
			
			if (sampledFact != null){
				
				// Add to the corresponding sampled-facts list
				if (sampledFact.isPositive())
					sampledPositiveFacts.add(sampledFact);
				else
					sampledNegativeFacts.add(sampledFact);
				
				// Remove the sampledFact from the copiedFacts
				copiedFacts.remove(sampledFact);
			}else{
				break;
			}
		}
		
		// Sort two Fact Array lists according to Weight property in ASC order.
		Collections.sort(sampledPositiveFacts, Collections.reverseOrder());
		Collections.sort(sampledNegativeFacts, Collections.reverseOrder());
		
		// Output the sampled positive and negative facts
		System.out.println("sampledPositiveFacts: " + sampledPositiveFacts.toString());
		System.out.println("sampledNegativeFacts: " + sampledNegativeFacts.toString());
		
		// Accumulate the weight
		if (ModelConfig.IS_DECLINE){
			if (ModelConfig.IS_DECLINE_ONLY_FOR_COPIES){
				
			}else{
				// Accumulate the total weights of positive facts
				for (int i=0; i< sampledPositiveFacts.size(); i++)
					positiveFactsWeight += sampledPositiveFacts.get(i).getWeight() 
							* Math.pow(ModelConfig.DECLINE_RATE, i);
					
				// Accumulate the total weights of negative facts
				for (int i=0; i< sampledNegativeFacts.size(); i++){
					Fact negativeFact = sampledNegativeFacts.get(i);
					
					if (negativeFact.isRetracted())
						negativeFactsWeight += negativeFact.getWeight() 
								* Math.pow(ModelConfig.DECLINE_RATE, i) * ModelConfig.RETRACTION_REDUCTION_RATIO;
					else
						negativeFactsWeight += negativeFact.getWeight() 
						* Math.pow(ModelConfig.DECLINE_RATE, i);
				}
					
			}
		}else{
			// Accumulate the total weights of positive facts
			for (int i=0; i< sampledPositiveFacts.size(); i++)
				positiveFactsWeight += sampledPositiveFacts.get(i).getWeight();
				
			// Accumulate the total weights of negative facts
			for (int i=0; i< sampledNegativeFacts.size(); i++){
				Fact negativeFact = sampledNegativeFacts.get(i);
				
				if (negativeFact.isRetracted())
					negativeFactsWeight += negativeFact.getWeight() * ModelConfig.RETRACTION_REDUCTION_RATIO;
				else
					negativeFactsWeight += negativeFact.getWeight();
			}
				
		}
		
		System.out.println("Positive Weight: " + positiveFactsWeight);
		System.out.println("Negative Weight: " + negativeFactsWeight);
		
		// Decide the attitude
		if (positiveFactsWeight >= negativeFactsWeight)
			this.isPositive = true;
		else
			this.isPositive = false;
			
		return this.isPositive;
	}
	
	// Update the facts in the head in terms of the life ticks
	public void updateFactsLife(){
		Iterator<Fact> iterator = this.getFacts().iterator();
		
		while(iterator.hasNext()){
			Fact fact = iterator.next();
			
			// Reduce one tick of life
			fact.reduceLifeTicksBy(1);
			
			// Remove the fact if tick life is 0
			if(fact.getLifeTicks() == 0){
				iterator.remove();
			}
		}
	}
	
	// Update the negative facts in the head in terms of the retraction tag.
	public void updateRetractionOfFacts(){
		for (Fact fact: this.getFacts()){
			if(!fact.isPositive()){
				// Add retraction tag for negative facts by some probability
				if(Math.random() < ModelConfig.PROB_OF_OCCURRENCE_OF_RETRATION_TAG) 
					fact.addRetractionTag();
			}
		}
	}
	
	// Create and Add new facts into the head
	public ArrayList<Fact> addNewFacts() {
		ArrayList<Fact> newFacts = Fact.createFacts(ModelConfig.MAXIMUM_NUMBER_OF_NEW_FACTS_EACH_TICK);

		return addFacts(newFacts);
	}
	
	// Add new facts into the head
	public ArrayList<Fact> addFacts(ArrayList<Fact> newFacts){
		ArrayList<Fact> addedFacts = new ArrayList<Fact>();
		
		for (Fact fact: newFacts){
			if (this.facts.size() < ModelConfig.MAXIMUM_NUMBER_OF_FACTS_IN_HEAD){
				this.facts.add(fact);
				addedFacts.add(fact);
			}
		}
		
		return addedFacts;
	}
	
	/*
	 * Getter and Setter
	 */
	public boolean isPositive() {
		return isPositive;
	}

	public ArrayList<Fact> getFacts() {
		return facts;
	}
	
	// Get the positive facts or negative facts in the head
	public ArrayList<Fact> getPositiveFacts (boolean isPostive){
		ArrayList<Fact> facts = new ArrayList<Fact>();
		
		if (isPostive){
			for (Fact fact: this.facts){
				if(fact.isPositive())
					facts.add(fact);
			}
		}else{
			for (Fact fact: this.facts){
				if(!fact.isPositive())
					facts.add(fact);
			}
		}
		
		return facts;
	}
	
	/*
	 * Helper Function
	 */
	
	// Select one fact from a fact list randomly according to the weight
	private static Fact selectOneFactRandomly(ArrayList<Fact> list){
		// Compute the total weight of all items together
		double totalWeight = 0.0;
		for (Fact i : list)
		{
		    totalWeight += i.getWeight();
		}
		
		// Now choose a random item
		int randomIndex = -1;
		double random = Math.random() * totalWeight;
		for (int i = 0; i < list.size(); i++)
		{
		    random -= list.get(i).getWeight();
		    
		    if (random <= 0.0)
		    {
		        randomIndex = i;
		        break;
		    }
		}
		
		if (randomIndex == -1)
			return null;
		else 
			return list.get(randomIndex);
	}
}
