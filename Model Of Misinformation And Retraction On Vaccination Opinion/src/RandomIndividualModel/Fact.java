package RandomIndividualModel;
import java.util.ArrayList;
import java.util.UUID;

public class Fact implements Comparable<Fact> {
	private String id;
	
	private int weight;
	private int lifeTicks;
	
	private boolean isRetracted;
	private boolean isPositive;

	public Fact(){
		this.id = UUID.randomUUID().toString();
		
		this.weight = ModelConfig.random.nextInt(ModelConfig.MAXIMUM_FACT_WEIGHT) + 1;
		this.lifeTicks = ModelConfig.random.nextInt(ModelConfig.MAXIMUM_FACT_LIFT_TICKS) + 1;
		
		this.isRetracted = false;
		this.isPositive = ModelConfig.random.nextBoolean();
	}
	
	// Create a certain length of ArrayList of facts 
	public static ArrayList<Fact> createFacts(int maximumNumber){
		int numberOfFacts = ModelConfig.random.nextInt(maximumNumber + 1);
		ArrayList<Fact> facts = new ArrayList<Fact> (numberOfFacts);
		
		for(int i=0; i < numberOfFacts; i++){
			facts.add(new Fact());
		}
		
		return facts;
	}
	
	// Add retraction tag to the fact
	public void addRetractionTag (){
		if (!this.isPositive)
			this.isRetracted = true;
	}
	
	// Reduce the lift time by a certain amount
	public void reduceLifeTicksBy(int ticks){
		this.lifeTicks -= ticks;
		
		if (this.lifeTicks < 0)
			this.lifeTicks = 0;
	}
	
	/*
	 *  Getter and Setter
	 */
	public int getWeight() {
		return weight;
	}
	
	public int getLifeTicks() {
		return lifeTicks;
	}

	public boolean isPositive() {
		return isPositive;
	}

	public boolean isRetracted() {
		return isRetracted;
	}
	
	/*
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Fact [id=" + id + ", weight=" + weight + ", lifeTicks=" + lifeTicks + ", isRetracted=" + isRetracted
				+ ", isPositive=" + isPositive + "]";
	}
	
	/*
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Fact fact) {
		return this.weight - fact.weight;
	}
	
	
}
