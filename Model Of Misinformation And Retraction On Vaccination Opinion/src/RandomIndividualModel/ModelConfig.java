package RandomIndividualModel;
import java.util.Random;

public class ModelConfig {
	// System-related constant
	public static final Random random = new Random();
	
	// Model-related constant
	public static final int MAXIMUM_NUMBER_OF_FACTS_IN_HEAD = 12;
	public static final int NUMBER_OF_SAMPLE = 4;
	
	public static final int MAXIMUM_FACT_LIFT_TICKS = 10;
	public static final int MAXIMUM_FACT_WEIGHT = 10;
	public static final int MAXIMUM_NUMBER_OF_NEW_FACTS_EACH_TICK = 5;
	
	public static final double RETRACTION_REDUCTION_RATIO = 0.8;
	public static final double PROB_OF_OCCURRENCE_OF_RETRATION_TAG = 0.2;
	
	public static final double DECLINE_RATE = 0.8;
	
	// Algorithm-related constant
	public static final boolean IS_DECLINE = false;
	public static final boolean IS_DECLINE_ONLY_FOR_COPIES = false;
	
	// Line-Chart related constant
	public static final int WINDOW_WIDTH = 1920;
	public static final int WINDOW_HEIGHT = 600;

}
