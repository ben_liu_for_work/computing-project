package RandomIndividualModel;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.BorderLayout;
import java.lang.Thread;
import java.util.ArrayList;

public class IndividualModelRun {
	public static XYSeries series;
	
	// Used for accumulation of positive and negative attitudes so far
	private static int positiveAttitudeTimes = 0;
	private static int negativeAttitudeTimes = 0;
	
	public static void main(String[] args) {
		Individual individual = new Individual();
		
		createChart();
		runIndividualModel(individual);
	}
	
	// Run the individual model forever
	private static void runIndividualModel(Individual individual){

		int tick = 0;
		while(true){
			System.out.println(String.format("At tick %d: ", tick));
			System.out.println(String.format("Facts: %s", individual.getFacts()));
			
			System.out.println(String.format("Positive Facts: %d, Negative Facts: %d", 
			individual.getPositiveFacts(true).size(), individual.getPositiveFacts(false).size()));
			
			// Decide the attitude
			boolean attitude = individual.decideAttitude();
			
			// Visualization of the model
			String attitudeString = String.format("isPositive is %s", Boolean.toString(attitude));
			System.out.println(attitudeString);
			
			// Update the line chart
			if (attitude){
				IndividualModelRun.series.add(tick, 1);
				positiveAttitudeTimes++;
			}
			else{
				IndividualModelRun.series.add(tick, 0);
				negativeAttitudeTimes++;
			}
			
			// Update the state
			individual.updateFactsLife();
			individual.updateRetractionOfFacts();
			ArrayList<Fact> newFacts =individual.addNewFacts();
			
			//Output the new Added Facts and accumulated attitude so far
			System.out.println(String.format("%d New Added Facts after tick %d: %s", newFacts.size(), tick, newFacts));
			System.out.println(String.format("Accumulated Attitude So Far. Postive: %d, Negative: %d", 
					positiveAttitudeTimes, negativeAttitudeTimes));
			System.out.println();
			
			tick++;
			
			// Prevent the system updated too quickly
			try {
				Thread.currentThread().sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	// Chart
	private static void createChart(){
		// Create and configure the window
		JFrame window = new JFrame();
		window.setTitle("Attitude Of Vaccination Opinion");
		window.setSize(ModelConfig.WINDOW_WIDTH, ModelConfig.WINDOW_HEIGHT);
		window.setLayout(new BorderLayout());
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Create the line graph
		XYSeries series = new XYSeries("Desion-Making Individually");
		XYSeriesCollection dataset = new XYSeriesCollection(series);
		JFreeChart chart = ChartFactory.createXYLineChart("Individual Desion-Making", 
				"Tick", "Attitude: 1--positive, 0--nagative", dataset);
		
		// Add the graph to the window
		window.add(new ChartPanel(chart), BorderLayout.CENTER);
		
		// Show the window
		window.setVisible(true);
		
		// Store the series in order to refer to later
		IndividualModelRun.series = series;
		
	}
}
